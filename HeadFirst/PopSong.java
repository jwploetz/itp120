public class PopSong {
    public static void main (String[] args) {
        int popNum = 99;
        String word = "bottles";

        while (popNum > 0) {

            if (popNum == 1) {
                word = "bottle"; //singular
            }

            System.out.println(popNum + " " + word + " of pop on the wall");
            System.out.println(popNum + " " + word + " of pop");
            System.out.println("Take one down");
            System.out.println("Pass is around");
            popNum --;

            if (popNum > 0) {
                System.out.println(popNum + " " + word + " of pop on the wall");
            } else {
                System.out.println("No more bottles of pop on the wall");

            } // end else
        } // end while loop
    } // end main method
} // end class