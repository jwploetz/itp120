import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class GameSaverTest {
    public static void main(String[] args) {
        GameCharacter JFK = new GameCharacter(50, "Giant", new String[] { "strong", "swole", "muscle" });
        GameCharacter Silas = new GameCharacter(200, "Elf", new String[] { "water", "frisbee" });
        GameCharacter Andrea = new GameCharacter(120, "Troll", new String[] { "Java", "unco" });
        GameCharacter Jack = new GameCharacter(150, "Elf", new String[] {"robot", "jig saw" });

        try {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("Game.ser"));
            os.writeObject(JFK);
            os.writeObject(Silas);
            os.writeObject(Andrea);
            os.writeObject(Jack);
            os.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        JFK = null;
        Silas = null;
        Andrea = null;
        Jack = null;

        try {
            ObjectInputStream is = new ObjectInputStream(new FileInputStream("Game.ser"));
            GameCharacter JFKRestore = (GameCharacter) is.readObject();
            GameCharacter SilasRestore = (GameCharacter) is.readObject();
            GameCharacter AndreaRestore = (GameCharacter) is.readObject();
            GameCharacter JackRestore = (GameCharacter) is.readObject();

            System.out.println("JFK's type: " + JFKRestore.getType());
            System.out.println("Silas's type: " + SilasRestore.getType());
            System.out.println("Andrea's type: " + AndreaRestore.getType());
            System.out.println("Jack's Type: " + JackRestore.getType());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}