import java.io.*;
import java.net.*;

public class DailyAdviceServer
{
    String[] adviceList = {"Take notes as you go.", "Don't burn yourself, Read the recipe all the way through before you start.", "Save bacon fat.", "Set your timer for a few minutes less than the called-for time.", "Season and taste as you go.", "Trust yourself!", "Add a little salt to everything. Yes, everything.", "Clean as you go.", "Never use damp oven mitts.", "Mince garlic by hand.", "The most versatile and important tool is a sharp chef’s knife.", "Learn all the different ways to cook an egg."};
    //^Custom advice

    public void go() {
        try {
            ServerSocket serverSock = new ServerSocket(4242);
            while (true)
            {
                Socket sock = serverSock.accept();

                PrintWriter writer = new PrintWriter(sock.getOutputStream());
                String advice = getAdvice();
                writer.println(advice);
                writer.close();
                System.out.println(advice);
            }
        } catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    private String getAdvice() {
        int random = (int) (Math.random() * adviceList.length);
        return adviceList[random];
    }

    public static void main(String[] args)
    {
        DailyAdviceServer server = new DailyAdviceServer();
        server.go();
    }

}
