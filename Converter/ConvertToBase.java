import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
import java.io.*;

public class ConvertToBase {
  JFrame theFrame;
  JPanel mainPanel;

  public static void main(String[] args) {
    //crr
    new ConvertToBase().buildGUI();

  }
  public void buildGUI() {
          Converter convert = new Converter();
          //Creating the Frame
          JFrame frame = new JFrame("BASED Converter");
          frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          frame.setSize(900, 200);

          //Creating the MenuBar and adding components
          JMenuBar mb = new JMenuBar();
          //Creating the panel at bottom and adding components
          JPanel panel = new JPanel(); // the panel is not visible in output
          JLabel label = new JLabel("Converted Text");
          label.setFont(new Font("Serif", Font.BOLD, 40));
          JTextField tf = new JTextField();
          tf.setFont(new Font("Helvitica", Font.BOLD, 40));
           // accepts upto 10 characters

                  class ConvHexListener implements ActionListener {
                        public void actionPerformed(ActionEvent a) {
                          String InputText = tf.getText();
                          String Input = InputText;
                          Input = Input.replaceAll("\\s", "");
                          label.setText(convert.ConvertToHex(Input));
                        }
                    }

                  class ConvBinListener implements ActionListener {
                        public void actionPerformed(ActionEvent a) {
                          String InputText = tf.getText();
                          String Input = InputText;
                          Input = Input.replaceAll("\\s", "");
                          label.setText(convert.ConvertToBinary(Input));
                        }
                    }

                    class ConvOctListener implements ActionListener {
                        public void actionPerformed(ActionEvent a) {
                          String InputText = tf.getText();
                          String Input = InputText;
                          Input = Input.replaceAll("\\s", "");
                          label.setText(convert.ConvertToOcto(Input));
                          label.setFont(new Font("Serif", Font.BOLD, 20));
                        }
                    }

                  class OctalIntListener implements ActionListener {
                        public void actionPerformed(ActionEvent a) {
                          String InputText = tf.getText();
                          String Input = InputText;
                          Input = Input.replaceAll("\\s", "");
                          label.setText(convert.ConvertToInt(Input, 8));
                        }
                    }

                    class BinarIntListener implements ActionListener {
                        public void actionPerformed(ActionEvent a) {
                          String InputText = tf.getText();
                          String Input = InputText;
                          Input = Input.replaceAll("\\s", "");
                          label.setText(convert.ConvertToInt(Input, 2));
                        }
                    }

                    class HexadIntListener implements ActionListener {
                        public void actionPerformed(ActionEvent a) {
                          String InputText = tf.getText();
                          String Input = InputText;
                          Input = Input.replaceAll("\\s", "");
                          label.setText(convert.ConvertToInt(Input, 16));
                        }
                    }
          JButton ConvHex = new JButton("ConvertToHex");
          ConvHex.addActionListener(new ConvHexListener());
          JButton ConvBin = new JButton("ConvertToBinary");
          ConvBin.addActionListener(new ConvBinListener());
          JButton ConvOct = new JButton("ConvertToOctal");
          ConvOct.addActionListener(new ConvOctListener());
          JButton OctalInt = new JButton("OctalToInt");
          OctalInt.addActionListener(new OctalIntListener());
          JButton BinarInt = new JButton("BinaryToInt");
          BinarInt.addActionListener(new BinarIntListener());
          JButton HexadInt = new JButton("HexToInt");
          HexadInt.addActionListener(new HexadIntListener());


          panel.add(ConvHex); // Components Added using Flow Layout
          panel.add(ConvBin);
          panel.add(ConvOct);
          panel.add(OctalInt);
          panel.add(BinarInt);
          panel.add(HexadInt);

          // Text Area at the Center
          JTextArea ta = new JTextArea("Enter Text");

          //Adding Components to the frame.
          frame.getContentPane().add(BorderLayout.SOUTH, panel);
          frame.getContentPane().add(BorderLayout.NORTH, label);
          frame.getContentPane().add(BorderLayout.CENTER, tf);
          frame.setVisible(true);

} // close method

    }
